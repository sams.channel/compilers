#!/bin/sh
docker run -h musl_compilers --rm --name musl_compilers -v $(pwd):/usr/musl_compilers -d musl_compilers && docker exec -it musl_compilers bash && docker stop musl_compilers
